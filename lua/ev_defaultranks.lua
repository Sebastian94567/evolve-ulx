/*-------------------------------------------------------------------------------------------------------------------------
	Default ranks
-------------------------------------------------------------------------------------------------------------------------*/

evolve.ranks.guest = {}
evolve.ranks.guest.Title = "Guest"
evolve.ranks.guest.Icon = "user_gray"
evolve.ranks.guest.NotRemovable = true
evolve.ranks.guest.UserGroup = "user"
evolve.ranks.guest.Color = Color(48,48,48)
evolve.ranks.guest.Immunity = 0
evolve.ranks.guest.Privileges = {}

evolve.ranks.member = {}
evolve.ranks.member.Title = "Member"
evolve.ranks.member.Icon = "user_orange"
evolve.ranks.member.NotRemovable = true
evolve.ranks.member.UserGroup = "member"
evolve.ranks.member.Color = Color(255,97,0)
evolve.ranks.member.Immunity = 1
evolve.ranks.member.Privileges = {}

evolve.ranks.veteran = {}
evolve.ranks.veteran.Title = "Veteran"
evolve.ranks.veteran.Icon = "user_add"
evolve.ranks.veteran.UserGroup = "veteran"
evolve.ranks.veteran.Color = Color(0,124,124)
evolve.ranks.veteran.Immunity = 2
evolve.ranks.veteran.Privileges = {}

evolve.ranks.trusted = {}
evolve.ranks.trusted.Title = "Trusted"
evolve.ranks.trusted.Icon = "user_green"
evolve.ranks.trusted.UserGroup = "trusted"
evolve.ranks.trusted.Color = Color(63,244,31)
evolve.ranks.trusted.Immunity = 3
evolve.ranks.trusted.Privileges = {}

evolve.ranks.vip = {}
evolve.ranks.vip.Title = "VIP"
evolve.ranks.vip.Icon = "user_red"
evolve.ranks.vip.UserGroup = "vip"
evolve.ranks.vip.Color = Color(0,255,0)
evolve.ranks.vip.Immunity = 4
evolve.ranks.vip.Privileges = {}

evolve.ranks.vip2 = {}
evolve.ranks.vip2.Title = "VIP+"
evolve.ranks.vip2.Icon = "user_add"
evolve.ranks.vip2.UserGroup = "vip+"
evolve.ranks.vip2.Color = Color(0,255,0)
evolve.ranks.vip2.Immunity = 5
evolve.ranks.vip2.Privileges = {}

evolve.ranks.moderator = {}
evolve.ranks.moderator.Title = "Moderator"
evolve.ranks.moderator.Icon = "shield_delete"
evolve.ranks.moderator.UserGroup = "moderator"
evolve.ranks.moderator.Color = Color(163,255,50)
evolve.ranks.moderator.Immunity = 20

evolve.ranks.admin = {}
evolve.ranks.admin.Title = "Admin"
evolve.ranks.admin.Icon = "shield"
evolve.ranks.admin.UserGroup = "admin"
evolve.ranks.admin.Color = Color(255,127,0)
evolve.ranks.admin.Immunity = 30

evolve.ranks.superadmin = {}
evolve.ranks.superadmin.Title = "Super Admin"
evolve.ranks.superadmin.Icon = "shield_add"
evolve.ranks.superadmin.UserGroup = "superadmin"
evolve.ranks.superadmin.Color = Color(233,0,255)
evolve.ranks.superadmin.Immunity = 50

evolve.ranks.owner = {}
evolve.ranks.owner.Title = "Owner"
evolve.ranks.owner.Icon = "key"
evolve.ranks.owner.ReadOnly = true
evolve.ranks.owner.UserGroup = "owner"
evolve.ranks.owner.Color = Color(211,42,42)
evolve.ranks.owner.Immunity = 99
